#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.2                                              #
#   Target     :   Download script postinstallazione sistema base   #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#              							                            #
#####################################################################

# --- Dichiarazione iniziale variabili --- #

Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Cyan='\e[0;36m'         # Ciano
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso
BCyan='\e[0;96m'        # Ciano Intenso
BWhite='\e[1;97m'       # Bianco Intenso

# --- Funzioni richiamate dai vari menu' --- #

Uscita (){
    clear
    echo " ################################################"
    echo " #          POSTINSTALL SCRIPT Ver. 0.2         #"
    echo " ################################################"
    echo 
    echo -e "${BYellow}  ------------------------------------------------${Color_Off}"
    echo -e "${BYellow}                   CONFERMA USCITA${Color_Off}"
    echo -e "${BYellow}  ------------------------------------------------${Color_Off}"
    echo 
    printf " Uscire dallo script (S/N) ? "
    read input
    if [ "$input" == "S" ] || [ "$input" == "s" ]; then
        clear
        exit 0
    fi
}

Menu (){
    clear
    echo " ################################################"
    echo " #          POSTINSTALL SCRIPT Ver. 0.2         #"
    echo " ################################################"
    echo 
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo -e "${BYellow}                 SELEZIONE PROFILO ${Color_Off}"
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo 
    echo -e "${BGreen} 1)${Color_Off} KDE"
    echo -e "${BGreen} 2)${Color_Off} Dwm"
    echo -e "${BGreen} 3)${Color_Off} Qtile"
    echo -e "${BGreen} 4)${Color_Off} Bspwm"
    echo 
    echo -e "${BYellow} 0)${Color_Off} Uscita Applicazione"
    echo 
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo 
    printf " Selezionare operazione : "
    read cod_menu
    case $cod_menu in
    1)
        echo -e "${BGreen} ::: Postinstallazione KDE :::${Color_Off}"
        echo 
        echo -e "${Green} Download script postinstallazione KDE ... ${Color_Off}"
        echo ""
        curl 'https://gitlab.com/undermod/bais/-/raw/master/script.sh' --output script.sh
        curl 'https://gitlab.com/undermod/bais/-/raw/master/kde.conf' --output kde.conf
        chmod +x script.sh
        echo 
        echo -e "${Green} Installazione selezione software ... ${Color_Off}"
        sleep 1
        ./script.sh k
        ;;
    2)
        echo -e "${BGreen} ::: Postinstallazione Dwm :::${Color_Off}"
        echo 
        echo -e "${Green} Download script postinstallazione Dwm ... ${Color_Off}"
        echo ""
        curl 'https://gitlab.com/undermod/bais/-/raw/master/script.sh' --output script.sh
        curl 'https://gitlab.com/undermod/bais/-/raw/master/dwm.conf' --output dwm.conf
        chmod +x script.sh
        echo 
        echo -e "${Green} Installazione selezione software ... ${Color_Off}"
        sleep 1
    	./script.sh d
        ;;
    3)
        echo -e "${BGreen} ::: Postinstallazione Qtile :::${Color_Off}"
        echo 
        echo -e "${Green} Download script postinstallazione Qtile ... ${Color_Off}"
        echo ""
        curl 'https://gitlab.com/undermod/bais/-/raw/master/script.sh' --output script.sh
        curl 'https://gitlab.com/undermod/bais/-/raw/master/qtile.conf' --output qtile.conf
        chmod +x script.sh
        echo 
        echo -e "${Green} Installazione selezione software ... ${Color_Off}"
        sleep 1
    	./script.sh q
        ;;
    4)
        echo -e "${BGreen} ::: Postinstallazione Bspwm :::${Color_Off}"
        echo 
        echo -e "${Green} Download script postinstallazione Bspwm ... ${Color_Off}"
        echo ""
        curl 'https://gitlab.com/undermod/bais/-/raw/master/script.sh' --output script.sh
        curl 'https://gitlab.com/undermod/bais/-/raw/master/bspwm.conf' --output bspwm.conf
        chmod +x script.sh
        echo 
        echo -e "${Green} Installazione selezione software ... ${Color_Off}"
        sleep 1
    	./script.sh b
        ;;
    0)
        echo "Uscita"
        Uscita
        ;;
    *)
        echo 
        echo -e "${Red} Scelta non riconusciuta${Color_Off}"
        read stop >> /dev/null
        ;;
    esac
}

# --- Parte principale dello script --- #

cod_menu=99
while [ "$cod_menu" -ne 0 ]
do
    Menu
done
