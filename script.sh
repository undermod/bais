#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.2                                              #
#   Target     :   Installazione DE / WM automatizzata              #
#                                                                   #
#####################################################################   
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#              							                            #
#####################################################################

# --- Dichiarazione iniziale variabili --- #

ver="0.2"
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Yellow='\e[0;33m'       # Giallo
Cyan='\e[0;36m'         # Ciano
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso
BCyan='\e[1;36m'        # Ciano Intenso

scelta=$1

# --- Controlli dipendenze  --- #

Notebook(){
    folder=$HOME"/Dev/dotfile"
    clear
    echo " ################################################"
    echo " #          POSTINSTALL SCRIPT Ver. $ver         #"
    echo " ################################################"
    echo 
    echo -e "${Yellow} ::: Stai utilizzando un notebook ?${Color_Off}"
    printf " (S/N) ==> "
    read notebook
    if [ "$notebook" == "S" ] || [ "$notebook" == "s" ]; then
        Touchpad
    fi
}

Configurazione(){
    folder=$(pwd)
    clear
    echo -e "${BCyan}:::   LETTURA FILE CONFIGURAZIONE SOFTWARE  :::${Color_Off}"
    sleep 0.5
    case $scelta in
        "k")
            echo -e "${Green}Installazione KDE ...${Color_Off}"
            echo
            display_manager_kde
            desktop_environment
            echo
            echo -e "${Green}Lettura configurazione KDE ...${Color_Off}"
            sleep 0.5
            conf="$folder/kde.conf"
            source $conf
            echo
            echo -e "${BGreen} +++   File configurazione software caricato   +++ ${Color_Off}"
            echo
            sleep 0.5
            nano $conf
            ;;
        "d")
            echo -e "${Green}Installazione Dwm ...${Color_Off}"
            echo
            display_manager
            dwm_window_manager
            echo
            echo -e "${Green}Lettura configurazione Dwm ...${Color_Off}"
            sleep 0.5
            conf="$folder/dwm.conf"
            source $conf
            echo
            echo -e "${BGreen} +++   File configurazione software caricato   +++ ${Color_Off}"
            echo
            sleep 0.5
            nano $conf
            Notebook
            ;;
        "q")
            echo -e "${Green}Installazione Qtile ...${Color_Off}"
            echo
            display_manager
            qtile_window_manager
            echo
            echo -e "${Green}Lettura configurazione Qtile ...${Color_Off}"
            sleep 0.5
            conf="$folder/qtile.conf"
            source $conf
            echo
            echo -e "${BGreen} +++   File configurazione software caricato   +++ ${Color_Off}"
            echo
            sleep 0.5
            nano $conf
            Notebook
            ;;
        "b")
            echo -e "${Green}Installazione Bspwm ...${Color_Off}"
            echo
            display_manager
            bspwm_window_manager
            echo
            echo -e "${Green}Lettura configurazione Bspwm ...${Color_Off}"
            sleep 0.5
            conf="$folder/bspwm.conf"
            source $conf
            echo
            echo -e "${BGreen} +++   File configurazione software caricato   +++ ${Color_Off}"
            echo
            sleep 0.5
            nano $conf
            Notebook
            ;;
    esac
    echo
    echo -e "${BGreen} :::   Verifica configurazione software caricata   ::: ${Color_Off}"
    echo
    sleep 0.5
}

# --- Funzioni generali di installazione --- #


#Installazione da Repository
install_repo (){
    echo
    for package in $@;
    do
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BYellow}!!!   ${index} --> Software già installato   !!!${Color_Off}"
            sleep 1
        else
            echo -e "${BGreen}==> Installazione $index${Color_Off}"
            echo
            sudo pacman -S --noconfirm --needed $package
            if pacman -Qi $package &> /dev/null
            then
                echo
                echo -e "${BGreen}+++   ${index}  --> Installazione riuscita   +++${Color_Off}"
                echo
            else
                echo
                echo -e "${BRed}!!!   ${index} --> Errore installazione   !!!${Color_Off}"
                echo
            fi
        fi
    done
    echo
    echo -e "${Green}+++   Installazione software  effettuata   +++${Color_Off}"
    echo
    sleep 1
}

#Installazione da AUR
install_aur (){
    echo
    for package in $@;
    do
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BYellow}!!!   ${index} --> Software già installato   !!!${Color_Off}"
            sleep 1
        else
            echo -e "${BGreen}==> Installazione $index${Color_Off}"
            echo
            paru -S --skipreview --noredownload $package
            if pacman -Qi $package &> /dev/null
            then
                echo
                echo -e "${BGreen}+++   ${index}  --> Installazione riuscita   +++${Color_Off}"
                echo
            else
                echo
                echo -e "${BRed}!!!   ${index} --> Errore installazione   !!!${Color_Off}"
                echo
            fi
        fi
    done
    echo
    echo -e "${Green}+++   Installazione software  effettuata   +++${Color_Off}"
    echo
    sleep 1
}


# --- Funzioni preliminari --- #

#Display Manager Lightdm
display_manager(){
    echo
    echo -e "${BCyan}:::   INSTALLAZIONE DISPLAY MANAGER   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Installazione Lighdm${Color_Off}"
    echo
    sudo pacman -S --noconfirm lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings
    echo
    echo -e "${BGreen}==> Attivazione Lighdm al boot${Color_Off}"
    echo
    sudo systemctl enable lightdm.service
    echo
    echo -e "${Green}+++   Display Manager installato   +++${Color_Off}"
    echo
    sleep 1
}

#Display Manager Sddm
display_manager_kde(){
    echo
    echo -e "${BCyan}:::   INSTALLAZIONE DISPLAY MANAGER   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Installazione Sddm${Color_Off}"
    echo
    sudo pacman -S --noconfirm sddm sddm-kcm
    echo
    echo -e "${BGreen}==> Attivazione Sddm al boot${Color_Off}"
    echo
    sudo systemctl enable sddm.service
    echo
    echo -e "${Green}+++   Display Manager installato   +++${Color_Off}"
    echo
    sleep 1
}

#dwm
dwm_window_manager(){
    echo
    echo -e "${BCyan}:::   INSTALLAZIONE WINDOW MANAGER   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Installazione Dwm${Color_Off}"
    echo
    mkdir $HOME/builds
    cd $HOME/builds
    git clone https://gitlab.com/undermod/dwm.git
    cd dwm
    sudo make clean install
    sudo pacman -S --noconfirm alacritty git
    echo
    echo -e "${Green}+++   Window Manager installato   +++${Color_Off}"
    echo
    echo -e "${BGreen}==> Creazione entry display manager${Color_Off}"
    echo
    if [ -d /usr/share/xsessions ]; then
        sudo makedir /usr/share/xsessions
    fi
    sudo echo '[Desktop Entry]' >> /usr/share/xsessions/dwm.desktop
    sudo echo 'Encoding=UTF-8' >> /usr/share/xsessions/dwm.desktop
    sudo echo 'Name=dwm' >> /usr/share/xsessions/dwm.desktop
    sudo echo 'Comment=Dynamic window manager' >> /usr/share/xsessions/dwm.desktop
    sudo echo 'Exec=dwm' >> /usr/share/xsessions/dwm.desktop
    sudo echo 'Icon=dwm' >> /usr/share/xsessions/dwm.desktop
    sudo echo 'Type=XSession' >> /usr/share/xsessions/dwm.desktop
    echo
    echo -e "${Green}+++   Entry display manager creata   +++${Color_Off}"
    echo
    sleep 1
}

#qtile
qtile_window_manager(){
    echo
    echo -e "${BCyan}:::   INSTALLAZIONE WINDOW MANAGER   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Installazione Qtile${Color_Off}"
    echo
    sudo pacman -S --noconfirm qtile alacritty git
    echo
    echo -e "${Green}+++   Window Manager installato   +++${Color_Off}"
    echo
    sleep 1
}

#bspwm
bspwm_window_manager(){
    echo
    echo -e "${BCyan}:::   INSTALLAZIONE WINDOW MANAGER   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Installazione Bspwm${Color_Off}"
    echo
    sudo pacman -S --noconfirm bspwm sxhkd alacritty git
    echo
    echo -e "${Green}+++   Window Manager installato   +++${Color_Off}"
    echo
    echo -e "${BGreen}==> Copia file di configurazione Bspwm${Color_Off}"
    echo
    install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
    install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc
    echo
    echo -e "${Green}+++   File di configurazione copiati   +++${Color_Off}"
    echo
    sleep 1
}

#kde
desktop_environment(){
    echo
    echo -e "${BCyan}:::   INSTALLAZIONE DESKTOP ENVIRONMENT   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Installazione KDE${Color_Off}"
    echo
    sudo pacman -S --noconfirm plasma plasma-wayland-session konsole dolphin git
    echo
    echo -e "${Green}+++   Desktop Environment installato   +++${Color_Off}"
    echo
    sleep 1
}

tuning_pacman (){
    clear
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo -e "${BYellow}                CONFIGURAZIONE PACMAN${Color_Off}"
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo
    echo -e "${BCyan}:::   ATTIVAZIONE DOWNLOADS PARALLELI   :::${Color_Off}"
    echo
    sudo sed -i 's/^#ParallelDownloads/ParallelDownloads/' /etc/pacman.conf
    echo
    echo -e "${BCyan}:::   ATTIVAZIONE VERBOSE PACKAGE LISTS   :::${Color_Off}"
    echo
    sudo sed -i 's/^#VerbosePkgLists/VerbosePkgLists/' /etc/pacman.conf
    echo
    echo -e "${BCyan}:::   ATTIVAZIONE COLOR   :::${Color_Off}"
    echo
    sudo sed -i 's/^#Color/Color/' /etc/pacman.conf
    echo
    echo -e "${Green}+++   Pacman aggiornato   +++${Color_Off}"
    echo
    echo
    echo -e "${BCyan}:::   AGGIORNAMENTO DATABASE PACCHETTI   :::${Color_Off}"
    echo
    sudo pacman -Sy
    echo
    echo -e "${Green}+++   Database pacchetti aggiornato   +++${Color_Off}"
    echo
    echo -e "${BGreen} *******************************************${Color_Off}"
    echo -e "${BGreen} >>>   Pacman configurato con successo   <<<${Color_Off}"
    echo -e "${BGreen} *******************************************${Color_Off}"
    echo 
    sleep 0.5
}

Git_conf (){
    clear
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo -e "${BYellow}              CONFIGURAZIONE ACCOUNT GIT${Color_Off}"
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo 
    printf " ==> Inserire nome utente GIT : "
    read nome
    printf " ==> Inserire mail utente GIT : "
    read mail
    git config --global user.name \"$nome\"
    git config --global user.email \"$mail\"
    echo 
    echo -e "${BGreen}*************************************************${Color_Off}"
    echo -e "${BGreen}>>>   CONFIGURAZIONE ACCOUNT GIT EFFETTUATA   <<<${Color_Off}"
    echo -e "${BGreen}*************************************************${Color_Off}"
    echo 
    sleep 0.5
}

Aur_helper (){
    package=paru
    clear
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo -e "${BYellow}              CONFIGURAZIONE AUR HELPER${Color_Off}"
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo 
    echo -e "${Green} ::: Installazione PARU :::${Color_Off}"
    echo 
    # --- Controllo che paru non sia già installato --- #
    if pacman -Qi $package &> /dev/null
    then
        echo -e "${BYellow}**********************************${Color_Off}"
        echo -e "${BYellow}***   Paru e' gia' installato   ***${Color_Off}"
        echo -e "${BYellow}**********************************${Color_Off}"
    else
        git clone https://aur.archlinux.org/paru.git
        cd paru
        makepkg -si
        cd ..
        rm -rf paru
        if sudo pacman -Qi $package &> /dev/null
        then
            echo -e "${BGreen}##############################################${Color_Off}"
            echo -e "${BGreen}>>> Paru e' stato installato con successo  <<<${Color_Off}"
            echo -e "${BGreen}##############################################${Color_Off}"
        else
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
            echo -e "${BRed}XXX   OPERAZIONE NON RIUSCITA   XXX${Color_Off}"
            echo -e "${BRed}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!${Color_Off}"
        fi
    fi
    sleep 0.5
}

Mirror (){
    folder=$(pwd)
    clear
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo -e "${BYellow}                OTTIMIZZAZIONE MIRROR${Color_Off}"
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo 
    echo -e "${BCyan}:::   BACKUP MIRROR   :::${Color_Off}"
    echo
    sudo cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
    echo
    echo -e "${BCyan}:::   SELEZIONE MIRROR   :::${Color_Off}"
    echo
    echo -e "${Green}==> Installazione reflector${Color_Off}"
    echo
    sudo pacman -S --noconfirm reflector
    echo
    echo -e "${Green}==> Generazione Lista Mirror x Italia${Color_Off}"
    echo
    sudo reflector --latest 5 -- country ${country} --sort rate --save /etc/pacman.d/mirrorlist
    echo
    echo -e "${Green}+++   Generazione Mirror Effettuata   +++${Color_Off}"
    echo
    echo -e "${BGreen}*********************************${Color_Off}"
    echo -e "${BGreen}>>> Mirror list ottimizzata   <<<${Color_Off}"
    echo -e "${BGreen}*********************************${Color_Off}"
    sleep 0.5
}

Shell(){
    clear
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo -e "${BYellow}                Installazione SHELL${Color_Off}"
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo 
    if [ "${fish_shell}" == 'S' ]
    then
        echo
        echo -e "${Green} :::   Installazione fish shell ...   :::${Color_Off}"
        echo
        sudo pacman -S fish
        echo
        echo -e "${Green} :::   Impostazione fish come shell predefinita ...   :::${Color_Off}"
        echo
        chsh -s /bin/fish
        echo
        echo -e "${Green} :::   Impostazione tema fish -> starship ...   :::${Color_Off}"
        echo
        sh -c "$(curl -fsSL https://starship.rs/install.sh)"
        echo "starship init fish | source" >> ~/.config/fish/config.fish
    fi
    if [ "${zsh_shell}" == 'S' ]
    then
        echo
        echo -e "${Green} :::   Installazione zsh shell ...   :::${Color_Off}"
        echo
        sudo pacman -S zsh
        echo
        echo -e "${Green} :::   Impostazione zsh come shell predefinita ...   :::${Color_Off}"
        echo
        chsh -s /bin/zsh
        echo
        echo -e "${Green} :::   Impostazione tema zsh -> starship ...   :::${Color_Off}"
        echo
        sh -c "$(curl -fsSL https://starship.rs/install.sh)"
        echo 'eval"$(starship init zsh)"' >> ~/.zshrc
    fi
    sleep 0.5
}

Touchpad(){
    clear
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo -e "${BYellow}            FIX CONFIGURAZIONE TOUCHPAD${Color_Off}"
    echo -e "${BYellow} ------------------------------------------------${Color_Off}"
    echo 
    if [[ -e /etc/X11/xorg.conf.d/10-synatics.conf ]]
    then
	    echo -e "{$BYellow}*******************************************{$Color_Off}"
	    echo -e "{$BYellow}***   FILE DI CONFIGURAZIONE PRESENTE   ***{$Color_Off}"
	    echo -e "{$BYellow}*******************************************{$Color_Off}"
    else
        echo -e "${Green} ::: Copia del file di configurazione del touchpad :::${Color_Off}"
        echo 
	    sudo cp settings/touchpad/10-synaptics.conf /etc/X11/xorg.conf.d/10-synaptics.conf
	    echo 
	    echo -e "${BGreen}############################################${Color_Off}"
	    echo -e "${BGreen}###   CONFIGURAZIONE TOUCHPAD AGGIORNATA ###${Color_Off}"
	    echo -e "${BGreen}############################################${Color_Off}"
    fi
    sleep 0.5
}

# --- Clonazione dotfile configurazione --- #

Dotfile(){
    echo 
    echo -e "${Green} ::: Check Undermod's Dotfile :::${Color_Off}"
    echo 
    if [[ ! -e $HOME/Dev ]]
    then
        mkdir -p $HOME/Dev
        echo -e "${BGreen} ==> Creata cartella DEV${Color_Off}"
    fi
    echo 
    echo -e "${Green} ::: Clonazione dei dotfile di undermod :::${Color_Off}"
    echo 
    if [[ ! -e $folder ]]
    then
        cd $HOME/Dev
        git clone https://gitlab.com/undermod/dotfile.git
        echo 
        echo -e "${BGreen} ==> Repository clonato con successo${Color_Off}"
        echo 
    else
        cd $folder
        git pull
        echo 
        echo -e "${BGreen} ==> Repository sincronizzato con successo${Color_Off}"
    fi
    echo    
}

# Rimozione script a fine installazione
pulizia(){
    echo
    echo -e "${Green} ::: Rimozione script :::${Color_Off}"
    echo
    if [ -d $HOME/bais_script]; then
        sudo rm -rf $HOME/bais_script
    else
        echo -e "${BRed}XXX   Impossibile rimuovere script. Cartella non presente   XXX${Color_Off}"
    fi
}

# --- Parte principale dello script --- #

clear
echo " ################################################"
echo " #          POSTINSTALL SCRIPT Ver. $ver         #"
echo " ################################################"
echo ""
echo -e "${BYellow} ------------------------------------------------${Color_Off}"
echo -e "${BYellow}          INSTALLAZIONE SELEZIONE SOFTWARE ${Color_Off}"
echo -e "${BYellow} ------------------------------------------------${Color_Off}"
echo ""
echo -e "${Yellow} Verranno installati tutti i software presenti nel${Color_Off}"
echo -e "${Yellow} file di configurazione dello script. Verificare${Color_Off}"
echo -e "${Yellow} il file prima di procedere${Color_Off}"
echo ""
echo -e " ------------------------------------------------ "
echo ""
printf " Sicuro di voler continuare ? (S/N) ==> "
read input
if [ "$input" == "S" ] || [ "$input" == "s" ]; then
    tuning_pacman
    Mirror
    Git_conf
    Configurazione
    Aur_helper
    Shell
    if [ $essential == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : ESSENTIAL   :::${Color_Off}"
        sleep 1
        install_repo ${essential_pkg[@]}
    fi
    if [ $essential_aur == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : ESSENTIAL AUR   :::${Color_Off}"
        sleep 1
        install_aur ${essential_aur_pkg[@]}
    fi
    if [ $network == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : NETWORK   :::${Color_Off}"
        sleep 1
        install_repo ${network_pkg[@]}
    fi
    if [ $network_aur == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : NETWORK AUR   :::${Color_Off}"
        sleep 1
        install_aur ${network_aur_pkg[@]}
    fi
    if [ $office == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : OFFICE   :::${Color_Off}"
        sleep 1
        install_repo ${office_pkg[@]}
    fi
    if [ $office_aur == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : OFFICE AUR  :::${Color_Off}"
        sleep 1
        install_aur ${office_aur_pkg[@]}
    fi
    if [ $devel == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : DEVELOPMENT   :::${Color_Off}"
        sleep 1
        install_repo ${devel_pkg[@]}
    fi
    if [ $devel_aur == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : DEVELOPMENT AUR  :::${Color_Off}"
        sleep 1
        install_aur ${devel_aur_pkg[@]}
    fi
    if [ $utility == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : UTILITY   :::${Color_Off}"
        sleep 1
        install_repo ${utility_pkg[@]}
    fi
    if [ $utility_aur == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : UTILITY AUR  :::${Color_Off}"
        sleep 1
        install_aur ${utility_aur_pkg[@]}
    fi
    if [ $media == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : MEDIA   :::${Color_Off}"
        sleep 1
        install_repo ${media_pkg[@]}
    fi
    if [ $media_aur == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : MEDIA AUR  :::${Color_Off}"
        sleep 1
        install_aur ${media_aur_pkg[@]}
    fi
    if [ $look == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : LOOK   :::${Color_Off}"
        sleep 1
        install_repo ${look_pkg[@]}
    fi
    if [ $look_aur == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : LOOK AUR  :::${Color_Off}"
        sleep 1
        install_aur ${look_aur_pkg[@]}
    fi
    if [ $eyecandy == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : EYECANDY   :::${Color_Off}"
        sleep 1
        install_repo ${eyecandy_pkg[@]}
    fi
    if [ $eyecandy_aur == "S" ];
    then
        echo
        echo -e "${BCyan}:::   INSTALLAZIONE CATEGORIA SOFTWARE : EYECANDY AUR  :::${Color_Off}"
        sleep 1
        install_aur ${eyecandy_aur_pkg[@]}
    fi
    #pulizia
    #Dotfile
exit 0
fi

# Inserire richiesta per installazione configurazione o meno
