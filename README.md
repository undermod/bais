# BAIS

Automated script for Arch Linux installation with Dwm,Qtile,Bspwm as window manager and KDE as Desktop environment

### Istruzioni

Localizzare la console in italiano

```
loadkeys it
```

scaricare il file "download.sh"

``` 
curl 'https://gitlab.com/undermod/bais/-/raw/master/download.sh' --output download.sh
```

rendere eseguidile il file scaricato

```
chmod +x download.sh
```

eseguire il file scaricato

```
./donwload.sh
```
