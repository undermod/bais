#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro                                   #
#   Versione   :   0.1                                              #
#   Target     :   Download e preparazione script BAIS              #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- # 

set -e
Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Cyan='\e[0;36m'         # Ciano
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso
BCyan='\e[0;96m'        # Ciano Intenso
BWhite='\e[1;97m'       # Bianco Intenso

# --- SCRIPT --- #

clear
echo -e "${BCyan}:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::${Color_Off}"
echo -e "${BCyan}:::   DOWNLOAD BAIS - BASE ARCH LINUX INSTALLATION SCRIPT ver 0.1   :::${Color_Off}"
echo -e "${BCyan}:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::${Color_Off}"
echo

# Scarico il file per l'installazione dal repository
echo -e "${Green}Download script${Color_Off}"
curl 'https://gitlab.com/undermod/bais/-/raw/master/bais.sh' --output bais.sh
echo
echo -e "${Green}Download configurazione script${Color_Off}"
curl 'https://gitlab.com/undermod/bais/-/raw/master/bais.conf' --output bais.conf

# Modifica del file di configurazione
clear
echo -e "${BCyan}:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::${Color_Off}"
echo -e "${BCyan}:::   DOWNLOAD BAIS - BASE ARCH LINUX INSTALLATION SCRIPT ver 0.1   :::${Color_Off}"
echo -e "${BCyan}:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::${Color_Off}"
echo
echo -e "${BYellow}Se necessario modificare il file di configurazione${Color_Off}"
echo -e "${BYellow}--------------------------------------------------${Color_Off}"
echo
printf "${Green}==> Premere INVIO per continuare${Color_Off}"
nano bais.conf

# Rendo eseguibile il file scaricato
echo
echo -e "${Green}Modifica permessi script${Color_Off}"
chmod +x bais.sh

# Lancio script Bais
echo
echo -e "${Green}Esecuzione script${Color_Off}"
./bais.sh
