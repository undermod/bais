#!/bin/bash

#####################################################################
#                                                                   #
#   Autore     :   Antonio Gistro aka Undermod                      #
#   Versione   :   0.1                                              #
#   Target     :   Installazione automatizzata sistema base         #
#                                                                   #
#####################################################################
#                                                                   #
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.  #
#                                                                   #
#####################################################################

# --- Dichiarazione iniziale variabili --- #

Color_Off='\e[0m'
Red='\e[0;31m'          # Rosso
Green='\e[0;32m'        # Verde
Cyan='\e[0;36m'         # Ciano
Yellow='\e[0;33m'       # Giallo
BRed='\e[1;31m'         # Rosso Intenso
BGreen='\e[1;32m'       # Verde Intenso
BYellow='\e[1;33m'      # Giallo Intenso
BCyan='\e[0;96m'        # Ciano Intenso
BWhite='\e[1;97m'       # Bianco Intenso

conf="bais.conf"

# --- Funzioni richiamate dai menu' --- #

# Verifico l'esistenza del file di configurazione
check_conf(){
    clear
    echo -e "${BWhite}#################################################################${Color_Off}"
    echo -e "${BWhite}#     BAIS - Base Arch Linux Installation Script - Ver. 0.1     #${Color_Off}"
    echo -e "${BWhite}#################################################################${Color_Off}"
    echo
    echo -e "${BCyan}:::   CHECK DIPENDENZE SCRIPT INSTALLAZIONE   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Check file configurazione${Color_Off}"
    if [[ -e $conf ]]
    then
        source $conf
        echo
        echo -e "${Green}+++   File configurazione importato correttamente  +++${Color_Off}"
        echo
    else
        echo
        echo -e "${BRed}#########################################################${Color_Off}"
        echo -e "${BRed}### File di configurazione non trovato. Uscita script ###${Color_Off}"
        echo -e "${BRed}#########################################################${Color_Off}"
        echo
        echo "File configurazione non trovato " >> error.log
        exit 1
    fi
    isvbox=$(lspci | grep "VirtualBox")
    if [ "${isvbox}" ];
    then
        echo -e "${BYellow}::: Installazione in VirtualBox rilevata :::${Color_Off}"
    fi
    isvmware=$(lspci | grep "VMware")
    if [ "${isvmware}" ];
    then
        echo -e "${BYellow}::: Installazione in VMWare rilevata :::${Color_Off}"
    fi
    sleep 1
    clear
}

#Check Connessione
connection(){
    echo
    echo -e "${BCyan}:::   CHECK CONNESSIONE INTERNET   :::${Color_Off}"
    echo
    ping -c 3 www.google.com
    if [[ $? = 0 ]]
    then
        echo
        echo -e "${Green}+++   Connessione prensente   +++${Color_Off}"
        echo
    else
        echo
        echo -e "${BRed}##########################################${Color_Off}"
        echo -e "${BRed}### Connessione assente. Uscita script ###${Color_Off}"
        echo -e "${BRed}##########################################${Color_Off}"
        echo
        echo "Connessione assente. Impossibile proseguire" >> error.log
        exit $?
    fi
    sleep 1
}

#Aggiornamento Orologio
clock_update(){
    echo
    echo -e "${BCyan}:::   AGGIORNAMENTO OROLOGIO   :::${Color_Off}"
    echo
    timedatectl set-ntp true
    echo
    echo -e "${Green}+++   Orologio aggiornato   +++${Color_Off}"
    echo
    sleep 1
}

#Pacman tuning
tuning_pacman(){
    echo
    echo -e "${BCyan}:::   ATTIVAZIONE DOWNLOADS PARALLELI   :::${Color_Off}"
    echo
    sed -i 's/^#ParallelDownloads/ParallelDownloads/' /etc/pacman.conf
    echo
    echo -e "${BCyan}:::   ATTIVAZIONE VERBOSE PACKAGE LISTS   :::${Color_Off}"
    echo
    sed -i 's/^#VerbosePkgLists/VerbosePkgLists/' /etc/pacman.conf
    echo
    echo -e "${BCyan}:::   ATTIVAZIONE COLOR   :::${Color_Off}"
    echo
    sed -i 's/^#Color/Color/' /etc/pacman.conf
    echo
    echo -e "${Green}+++   Pacman aggiornato   +++${Color_Off}"
    echo
    echo
    echo -e "${BCyan}:::   AGGIORNAMENTO DATABASE PACCHETTI   :::${Color_Off}"
    echo
    pacman -Sy
    echo
    echo -e "${Green}+++   Database pacchetti aggiornato   +++${Color_Off}"
    echo
    sleep 1
}

#Selezione Mirror
mirror_select(){
    #iso=$(curl -4 ifconfig.co/country-iso)
    echo
    echo -e "${BCyan}:::   BACKUP MIRROR   :::${Color_Off}"
    echo
    cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
    echo
    echo -e "${Green}+++   Backup mirror effettuato   +++${Color_Off}"
    sleep 0.5
    echo
    echo -e "${BCyan}:::   SELEZIONE MIRROR   :::${Color_Off}"
    echo
    echo -e "${Green}==> Installazione reflector${Color_Off}"
    echo
    pacman -S --noconfirm reflector
    echo
    echo -e "${Green}==> Generazione Lista Mirror x Italia${Color_Off}"
    echo
    reflector --latest 5 -- country ${country} --sort rate --save /etc/pacman.d/mirrorlist
    echo
    echo -e "${Green}+++   Generazione Mirror Effettuata   +++${Color_Off}"
    echo
    sleep 1
}

#Prearazione Hard Disk
hd_prepare(){
    echo
    echo -e "${BCyan}:::   PREPARAZIONE HARD DISK   :::${Color_Off}"
    echo
    echo -e "${Green}==> Installazione gptfdisk${Color_Off}"
    echo
    pacman -S --noconfirm gptfdisk
    echo
    echo -e "${Green}==> Selezione disco${Color_Off}"
    echo
    lsblk -n --output TYPE,KNAME,SIZE | awk '$1=="disk"{print NR") ","/dev/"$2"  -  "$3}'
    echo
    echo "Seleziona il disco da utilizzare (es: /dev/sda) ==> "
    read DISK
    echo 
    echo -e "${Green}==> Zap ed allineamento disco${Color_Off}"
    echo
    sgdisk -Z ${DISK} # zap all on disk
    sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment
    echo
    echo -e "${Green}==> Creazione partizioni${Color_Off}"
    echo
    sgdisk -n 1::+${boot_size} --typecode=1:ef00 --change-name=1:'EFIBOOT' ${DISK} # partition 1 (UEFI Boot Partition)
    sgdisk -n 2::+${swap_size} --typecode=2:8200 --change-name=2:'SWAP' ${DISK} # partition 2 (Swap), default start, remaining
    if [ $sep_home == 'True' ]; then
        sgdisk -n 3::+${root_size} --typecode=3:8300 --change-name=3:'ROOT' ${DISK} # partition 3 (Root), default start, remaining
        sgdisk -n 4::-0 --typecode=4:8300 --change-name=4:'HOME' ${DISK} # partition 3 (Root), default start, remaining
    else
        sgdisk -n 3::-0 --typecode=3:8300 --change-name=3:'ROOT' ${DISK} # partition 4 (Home), default start, remaining
    fi
    echo
    echo -e "${Green}+++   Partizionamento Effettuato   +++${Color_Off}"
    echo
    sleep 1
}

#Formattazione partizioni
format_partition(){
    partizione1=${DISK}1
    partizione2=${DISK}2
    partizione3=${DISK}3
    partizione4=${DISK}4
    echo
    echo -e "${BCyan}:::   FORMATTAZIONE PARTIZIONI   :::${Color_Off}"
    echo
    echo -e "${Green}==> Formattazione partizione boot (sda1)${Color_Off}"
    echo
    mkfs.fat -F32 -n "EFIBOOT" ${partizione1}
    echo
    echo -e "${Green}==> Formattazione partizione swap (sda2)${Color_Off}"
    echo
    mkswap ${partizione2}
    swapon ${partizione2}
    echo
    echo -e "${Green}==> Formattazione partizione root (sda3)${Color_Off}"
    echo
    mkfs.ext4 -L ROOT ${partizione3}
    echo
    if [ $sep_home == 'True' ]; then
        echo -e "${Green}==> Formattazione partizione home (sda4)${Color_Off}"
        echo
        mkfs.ext4 -L HOME ${partizione4}
        echo
    fi
    echo -e "${Green}+++   Formattazione Effettuata   +++${Color_Off}"
    echo
    sleep 1
}

#Montaggio Partizioni
mount_partition(){
    echo
    echo -e "${BCyan}:::   MONTAGGIO PARTIZIONI   :::${Color_Off}"
    echo
    echo -e "${Green}==> Montaggio partizione root (sda3)${Color_Off}"
    echo
    mount -t ext4 ${partizione3} /mnt
    echo
    echo -e "${Green}==> Montaggio partizione boot (sda1)${Color_Off}"
    echo
    mkdir /mnt/boot
    mount -t vfat ${partizione1} /mnt/boot/
    echo
    echo -e "${Green}+++   Montaggio Partizioni Effettuato   +++${Color_Off}"
    echo
    sleep 1
}

#Installazione Sistema Base
base_install(){
    echo
    echo -e "${BCyan}:::   INSTALLAZIONE SISTEMA BASE   :::${Color_Off}"
    echo
    pacstrap /mnt base base-devel linux-zen linux-firmware nano git
    echo
    echo -e "${Green}+++   Sistema Base Installato   +++${Color_Off}"
    echo
    sleep 1
}

#Generazione File Fstab
fstab_generation(){
    echo
    echo -e "${BCyan}:::   GENERAZIONE FILE FSTAB   :::${Color_Off}"
    echo
    genfstab -U /mnt >> /mnt/etc/fstab
    echo
    echo -e "${Green}+++   File FSTAB Generato   +++${Color_Off}"
    echo
    sleep 1
}

#Chroot
chrooting(){
    echo
    echo -e "${BCyan}:::   CHROOT NUOVO SISTEMA   :::${Color_Off}"
    echo

}

#TimeZone e Orologio di Sistema
time_config(){
    echo
    echo -e "${BCyan}:::   IMPOSTAZIONE TIMEZONE E OROLOGIO DI SISTEMA   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Impostazione Timezone${Color_Off}"
    echo
    arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Rome /etc/localtime
    echo
    echo -e "${BGreen}==> Impostazione Orologio di Sistema${Color_Off}"
    echo
    arch-chroot /mnt hwclock --systohc
    echo
    echo -e "${Green}+++   Timezone e Orologio Sistema Impostati   +++${Color_Off}"
    echo
    sleep 1
}

#Generazione locale
locale(){
    echo
    echo -e "${BCyan}:::   GENERAZIONE LOCALE   :::${Color_Off}"
    echo
    echo -e "${BYellow}==> Selezione lingua italiana${Color_Off}"
    echo
    arch-chroot /mnt sed -i 's/#it_IT.UTF-8 UTF-8/it_IT.UTF-8 UTF-8/' /etc/locale.gen
    echo
    echo -e "${BGreen}==> Generazione locale${Color_Off}"
    echo
    arch-chroot /mnt locale-gen
    echo LANG=it_IT.UTF-8 > /mnt/etc/locale.conf
    echo
    echo -e "${Green}+++   Locale Generato   +++${Color_Off}"
    echo
    sleep 1
}

#Mappatura tasteriera e Font console
console(){
    echo
    echo -e "${BCyan}:::   MAPPATURA CONSOLE   :::${Color_Off}"
    echo
    echo "KEYMAP=it" > /mnt/etc/vconsole.conf
    echo "FONT=Lat2-Terminus16" >> /mnt/etc/vconsole.conf
    echo
    echo -e "${Green}+++   Mappatura Effettuata   +++${Color_Off}"
    echo
    sleep 1
}

#Network configuration
hostname_config(){
    echo
    echo -e "${BCyan}:::   IMPOSTAZIONE HOSTNAME   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Impostazione Hostname${Color_Off}"
    echo
    echo $host_name >> /mnt/etc/hostname
    echo
    echo -e "${BGreen}==> Impostazione file hosts${Color_Off}"
    echo
    echo "127.0.1.1     "$host_name".localdomain    "$host_name >> /mnt/etc/hosts
    echo
    echo -e "${Green}+++   Hostname configurato   +++${Color_Off}"
    echo
    sleep 1
}

#Impostazione password Root
root_pswd(){
    echo
    echo -e "${BCyan}:::   IMPOSTAZIONE PASSWORD ROOT   :::${Color_Off}"
    echo
    printf "$root_password\n$root_password" | arch-chroot /mnt passwd
    echo
    echo -e "${Green}+++   Password Root impostata   +++${Color_Off}"
    echo
    sleep 1
}

#Microcode
microcode(){
    echo
    echo -e "${BCyan}:::   INSTALLAZIONE MICROCODE   :::${Color_Off}"
    echo
    proc_type=$(lscpu)
    if grep -E "GenuineIntel" <<< ${proc_type}; then
        echo -e "${BGreen}==> Installazione Intel microcode${Color_Off}"
        echo
        arch-chroot /mnt pacman -S --noconfirm intel-ucode
        echo
        echo -e "${Green}+++   Intel microcode installato   +++${Color_Off}"
        echo
        sleep 1
    elif grep -E "AuthenticAMD" <<< ${proc_type}; then
        echo -e "${BGreen}==> Installazione AMD microcode${Color_Off}"
        echo
        arch-chroot /mnt pacman -S --noconfirm amd-ucode
        echo
        echo -e "${Green}+++   AMD microcode installato   +++${Color_Off}"
        echo
        sleep 1
    fi
}

#Grub
bootloader(){
    echo
    echo -e "${BCyan}:::   INSTALLAZIONE BOOTLOADER   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Installazione Grub${Color_Off}"
    echo
    arch-chroot /mnt pacman -S --noconfirm grub dosfstools efibootmgr
    echo
    echo -e "${BGreen}==> Generazione voci EFI${Color_Off}"
    echo
    arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=arch --recheck --debug
    if [ "${isvbox}" ]; then
        echo
        echo -e "${BGreen}==> VirtualBox rilevata, crazione startup.nsh ${Color_Off}"
        echo
        echo "\EFI\arch\grubx64.efi" > /mnt/boot/startup.nsh
    fi
    echo
    echo -e "${BGreen}==> Generazione file di configurazione${Color_Off}"
    echo
    arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
    echo
    echo -e "${Green}+++   Bootloader installato   +++${Color_Off}"
    echo
    sleep 1
}

#Creazione Utente
user(){
    echo
    echo -e "${BCyan}:::   CREAZIONE NUOVO UTENTE   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Creazione nuovo utente${Color_Off}"
    echo
    arch-chroot /mnt  useradd -m -G wheel -s /bin/bash $user_name
    echo
    echo -e "${BGreen}==> Impostazione password nuovo utente${Color_Off}"
    echo
    printf "$user_password\n$user_password" | arch-chroot /mnt passwd $user_name
    echo
    echo -e "${BGreen}==> Gestione permessi nuovo utente${Color_Off}"
    echo
    sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /mnt/etc/sudoers
    echo
    echo -e "${BGreen}==> Creazione cartelle nuovo utente${Color_Off}"
    echo
    arch-chroot /mnt pacman -S --noconfirm xdg-user-dirs
    arch-chroot /mnt xdg-user-dirs-update
    echo
    echo -e "${Green}+++   Nuovo utente creato   +++${Color_Off}"
    echo
    sleep 1
}

#Audio
audio(){
    echo
    echo -e "${BCyan}:::   INSTALLAZIONE MODULO AUDIO   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Installazione pulseaudio${Color_Off}"
    echo
    arch-chroot /mnt pacman -S --noconfirm pulseaudio pulseaudio-alsa pamixer pavucontrol
    echo
    echo -e "${Green}+++   Modulo gestione audio installato   +++${Color_Off}"
    echo
    sleep 1
}

#Server Grafico Xorg
server_grafico(){
    echo
    echo -e "${BCyan}:::   INSTALLAZIONE SERVER GRAFICO   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Installazione Xorg${Color_Off}"
    echo
    arch-chroot /mnt pacman -S --noconfirm xorg-server xorg-apps xorg-xinit
    echo
    if lspci | grep VGA | grep AMD; then
        echo -e "${BGreen}==> Installazione Driver Video AMD${Color_Off}"
        echo
        arch-chroot /mnt pacman -S --noconfirm xf86-video-amdgpu mesa
    fi
    if lspci | grep VGA | grep ATI; then
        echo -e "${BGreen}==> Installazione Driver Video ATI${Color_Off}"
        echo
        arch-chroot /mnt pacman -S --noconfirm xf86-video-ati mesa
    fi
    if lspci | grep VGA | grep Intel; then
        echo -e "${BGreen}==> Installazione Driver Video Intel${Color_Off}"
        echo
        arch-chroot /mnt pacman -S --noconfirm xf86-video-intel mesa
    fi
    if lspci | grep VGA | grep VMware; then
        echo -e "${BGreen}==> Installazione Driver Video VMware${Color_Off}"
        echo
        arch-chroot /mnt pacman -S --noconfirm xf86-video-vmware mesa
    fi
    echo
    echo -e "${BGreen}==> Configurazione Tastiera${Color_Off}"
    echo
    echo 'Section "InputClass"' > /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
    echo '        Identifier "system-keyboard"' >> /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
    echo '        MatchIsKeyboard "on"' >> /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
    echo '        Option "XkbLayout" "it"' >> /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
    echo 'EndSection' >> /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
    echo
    echo -e "${BGreen}==> Installazione Driver Touchpad${Color_Off}"
    echo
    arch-chroot /mnt pacman -S --noconfirm xf86-input-synaptics
    echo
    echo -e "${Green}+++   Server Grafico installato   +++${Color_Off}"
    echo
    sleep 1
}

#Network Manager
network_manager(){
    echo
    echo -e "${BCyan}:::   INSTALLAZIONE GESTORE DI RETE   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Installazione Network Manager${Color_Off}"
    echo
    arch-chroot /mnt pacman -S --noconfirm networkmanager
    echo
    echo -e "${BGreen}==> Attivazione Network Manager al boot${Color_Off}"
    echo
    arch-chroot /mnt systemctl enable NetworkManager
    arch-chroot /mnt systemctl start NetworkManager
    echo
    echo -e "${Green}+++   Network Manager installato   +++${Color_Off}"
    echo
    sleep 1
}

#copia dello script postinstallazione
scritp_cloning(){
    echo
    echo -e "${BCyan}:::   COPIA SCRIPT NELLA HOME UTENTE NUOVO SISTEMA   :::${Color_Off}"
    echo
    curl 'https://gitlab.com/undermod/bais/-/raw/master/download_postinstall.sh' --output download_postinstall.sh
    mkdir /mnt/home/$user_name/bais-script
    cp download_postinstall.sh /mnt/home/$user_name/bais-script/download_postinstall.sh
    chmod -R a=rwx /mnt/home/$user_name/bais-script/
    echo
    echo -e "${Green}+++   Copia effettuata   +++${Color_Off}"
    echo
    sleep 1
}

#Smontare partizioni
ending(){
    echo
    echo -e "${BCyan}:::   CHIUSURA INSTALLAZIONE   :::${Color_Off}"
    echo
    echo -e "${BGreen}==> Smontaggio Partizioni${Color_Off}"
    echo
    umount -R /mnt
    echo
    echo -e "${Green}+++   Partizioni smontate   +++${Color_Off}"
    echo
    sleep 1
}

#Reboot
riavvio(){
    echo
    echo -e "${BCyan}:::   INSTALLAZIONE TERMINATA   :::${Color_Off}"
    echo
    echo -e "${BRed} Eliminare il supporto usato per l'installazione ${Color_Off}"
    echo -e "${BRed} ----------------------------------------------- ${Color_Off}"
    echo
    echo -e "${BYellow} +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${Color_Off}"
    echo -e "${BYellow} + Al riavvio effettuare il login con l'utente creato ed avviare lo script +${Color_Off}"
    echo -e "${BYellow} + presente nella cartella bais-script all'interno della prorpia home      +${Color_Off}"
    echo -e "${BYellow} +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${Color_Off}"
    echo
    echo -e "${BGreen}==> Premere un tasto per riavviare${Color_Off}"
    echo
    read input &> /dev/null
    reboot
}

# Funzione principale che richiama i vari step da eseguire
main (){
    clear
    echo -e "${BWhite}#################################################################${Color_Off}"
    echo -e "${BWhite}#     BAIS - Base Arch Linux Installation Script - Ver. 0.1     #${Color_Off}"
    echo -e "${BWhite}#################################################################${Color_Off}"
    echo
    check_conf
    connection
    clock_update
    mirror_select
    tuning_pacman
    hd_prepare
    format_partition
    mount_partition
    base_install
    fstab_generation
    chrooting
    time_config
    locale
    console
    hostname_config
    root_pswd
    microcode
    bootloader
    user
    audio
    server_grafico
    network_manager
    scritp_cloning
    ending
    riavvio
}

# --- Parte principale dello script --- #
clear
echo -e "${BWhite}#################################################################${Color_Off}"
echo -e "${BWhite}#     BAIS - Base Arch Linux Installation Script - Ver. 0.1     #${Color_Off}"
echo -e "${BWhite}#################################################################${Color_Off}"
echo
echo -e "${BYellow}Proseguendo tutti i dati presenti sul vostro hd verranno persi${Color_Off}"
echo
printf "Si desidera continuare ? (S/N) ==> "
read input
if [ "$input" == "S" ] || [ "$input" == "s" ]; then
        main
fi
